/**
 * Rutas Colección
 */

//var RutasDB = localStorage;
//var nextId = 0;
var NEW = 100;
var DEL = 101;
var CHG = 102;

var Rutas = Backbone.Collection.extend({
	model : Ruta,
	initialize : function() {
		this.on("add", function(model, collection, options) {
			console.log('Rutas:add ' + model.id);
			this.SavingColection(model, NEW);
		});
		this.on("remove", function(model, collection, options) {
			console.log('Rutas:remove ' + model.id);
			this.SavingColection(model, DEL);
		});
		this.on("change", function(model, value, options) {
			console.log('Rutas:change ' + model.id);
			this.SavingColection(model, CHG);
		});
	},
	importDB: function()
	{
		if (RutasDB.hasOwnProperty('Rutas'))			
			{
			  console.log('hay en localStorage Colección de Rutas');
              // Si existe Colección la detruyo y la vuelvo a crear ....
			  // Si hay tiempo pensar solo en añadir / quitar ruta concreta, no toda la colección  
			  if (rutas.length > 0)
			      rutas.reset();
			  rutas.add(JSON.parse(localStorage.getItem('Rutas')))  
			}
		else
			{
			  console.log('localStorage no tiene Rutas');
			}
	},
	SavingColection: function(model, action)
	{
		console.log('Evento de tipo ' + action);	
		if (RutasDB.hasOwnProperty('Rutas'))	
			{
			RutasDB.removeItem('Rutas');		
			}
		RutasDB.setItem('Rutas', JSON.stringify(rutas));
		//RutasDB.setItem('Rutas', JSON.stringify(rutas));
	}
});


//function  SavingColection(model, col, opt)
//{
//	console.log('PRUEBA ... id => ' + model.get('id') + ' que tengo ' + model.attributes);	
//	if (RutasDB.hasOwnProperty('Rutas'))	
//		{
//		RutasDB.removeItem('Rutas');		
//		}
//	RutasDB.setItem('Rutas', JSON.stringify(rutas));
//}

//var RutasDB = localStorage;
//var nextId = 0;

var rutas = new Rutas();

rutas.importDB();
$(document).on('pageinit', '#pgCasa', function() {
	var vistaNuevaRuta = new NuevaRuta({collection: rutas, el: '#pgCasa'});
	});

$(document).on('pageinit', '#pgMisRutas', function() {
	var vistaEditarRuta = new EditarRuta({collection: rutas, el: '#pgEditarRuta'});
	var vistaListaRutas = new ListaRutas({collection: rutas, el: '#pnRutas'});
	vistaListaRutas.editarRuta(vistaEditarRuta);
});

$(document).on('pageinit', '#pgMapa', function() {
	var vistaMapa = new Mapa({collection: rutas, el: '#pgMapa'});
	});
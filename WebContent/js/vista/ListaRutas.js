var ListaRutas = Backbone.View
		.extend({
			initialize : function() {
				var self = this;
				var newValue = null;
				var oldValue = null;
				this.collection.on('add', function(model, collection, options) {
					self.render(model, NEW);
				});
				this.collection.on('remove', function(model, collection,
						options) {
					self.render(model, DEL);
				});
				this.collection.on('change:titulo', function(model, value,
						options) {
					self.render(model, CHG);
				});
				this.render();
			},
			render : function(model, type) {
				var self = this;
				switch (type) {
				case CHG:
					// Solo cambio el título de la entrada que edito
					self.$('li #' + model.attributes.id).outerText = model.attributes.titulo;
					self.render();
					break;
				case NEW:
					// SOLO añado la nueva entrada
					var str = '<li><a id="' + model.get('id') + '" href="#">'	+ model.get('titulo') + '</a></li>';
			        this.$el.find('[data-role="listview"]').append(str);
					self.render();
					break;
				case DEL:
					// Borro el elemento seleccionado
					this.$el.find('li #' + model.attributes.id).remove();
					self.render();
					break;
				default:
					this.$el.html('<ul data-role="listview" data-filter="true"></ul>');
					// pintar rutas
					for (var i = 0; i < this.collection.size(); i++) {
						var m = this.collection.at(i);
						var str = '<li><a id="' + m.id + '" href="#">'
								+ m.get('titulo') + '</a></li>'
						this.$el.find('[data-role="listview"]').append(str);
					}
					this.$el.find('[data-role="listview"]').listview();
				}

			},
			editarRuta : function(vistaEditarRuta) {
				this.vistaEditarRuta = vistaEditarRuta;
			},
			abrirEditarRuta : function(e) {
				// recuperar id
				var id = $(e.target).attr('id');
				console.log('abrirEditarRuta (' + id + ')');
				this.vistaEditarRuta.model = this.collection.get(id);
				$(':mobile-pagecontainer').pagecontainer('change',
						'#pgEditarRuta');
				this.vistaEditarRuta.render();
			},
			events : {
				'click a' : 'abrirEditarRuta'
			}
		});
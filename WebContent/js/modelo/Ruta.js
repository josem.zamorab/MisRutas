/**
 * Rutas individuales
 */
var RutasDB = localStorage;
var nextId = 0;
var Ruta = Backbone.Model.extend({
	initialize : function() {
		// Si utilizamos persistencia, debermos cmabiar esto ....
		if (!this.id)
			this.set('id', _.uniqueId());
		if (!this.has("posiciones"))
			this.set('posiciones', []);
		if (!this.has("fecha"))
			this.set('fecha', Date());
	},
	defaults : {
		titulo : 'Ruta ',// + this.get('id'),//'Undefined',
		visible : 'on',
		color : '#000000',
	},
});
